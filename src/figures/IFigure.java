package figures;

public interface IFigure {
	double area();
	double perimeter();
}