package figures;

public class Main {
	public static void main(String[] args) {


		Circle c1 = new Circle(12.12);
		Circle c2 = new Circle(12.12);
		System.out.println(c1.toString());
		System.out.println(c1.hashcode());
		System.out.println(c2.hashcode());
		Circle c3 = new ColorCircle(12.12,"red");
		
		System.out.println(c1.equals(c2));
		System.out.println(c2.equals(c2));
		System.out.println(c3.equals(c2));
	}
}
