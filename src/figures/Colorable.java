package figures;

public interface Colorable {
	String  color();
}
