package figures;

public class Triangle implements IFigure {
		private double side1;
		private double side2;
		private double side3;
		
		public double area() {
			double p;
			p = perimeter()/2;
			return Math.pow(p*(p-side1)*(p-side2)*(p-side3), 1/2);
		}
		
		public double perimeter() {
			return side1 + side2 + side3;
		}

		@Override
		public String toString() {
			return "Triangle [side1=" + side1 + ", side2=" + side2 + ", side3=" + side3 + "]";
		}

		public Triangle(double side1, double side2, double side3) {
			this.side1 = Math.abs(side1);
			this.side2 = Math.abs(side2);
			this.side3 = Math.abs(side3);
		}

		public int hashcode() {
			int result;
			Double temp;
			temp = new Double(side1);
			result = temp.hashCode();
			temp = new Double(side2);
			result = result + temp.hashCode();
			temp = new Double(side3);
			result = result + temp.hashCode();

			return result;
		}

		public boolean equals(Triangle other) {
			if (this.side1 != other.side1) return false;
			if (this.side2 != other.side2) return false;
			if (this.side3 != other.side3) return false;
			return true;
		}
}
