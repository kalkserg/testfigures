package figures;

public class Rectangle implements IFigure {

		private double side1;
		private double side2;
		
		public double area() {
			return side1 * side2;
		}
		
		public double perimeter() {
			return 2*(side1 + side2);
		}

		public Rectangle(double side1, double side2) {
			this.side1 = Math.abs(side1);
			this.side2 = Math.abs(side2);
		}
		
		@Override
		public String toString() {
			return "Rectangle [side1=" + side1 + ", side2=" + side2 + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(side1);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(side2);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Rectangle other = (Rectangle) obj;
			if (Double.doubleToLongBits(side1) != Double.doubleToLongBits(other.side1))
				return false;
			if (Double.doubleToLongBits(side2) != Double.doubleToLongBits(other.side2))
				return false;
			return true;
		}
}