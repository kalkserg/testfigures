package figures;

public class ColorCircle extends Circle implements Colorable {
	private String color;
	

	public String color() {
		return color;
	}
	
	public ColorCircle(double radius, String color) {
		super(radius);
		this.color = color;
	}
	
	
	public int hashcode() {
		return super.hashcode() + color.hashCode();
	}

	public boolean equals(ColorCircle other) {
		if (!super.equals(other)) return false;
		if (color.compareToIgnoreCase(other.color)!=0) return false;

		return true;
	}
}
