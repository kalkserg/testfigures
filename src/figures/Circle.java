package figures;

public class Circle implements IFigure {
	private double radius;
	
	public double area() {
		return 2*radius*Math.PI;
	}
	
	public double perimeter() {
		return Math.PI * Math.pow(radius, 2);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}

	public Circle(double radius) {
		this.radius = Math.abs(radius);
	}

	public int hashcode() {
		Double result = new Double(radius);
		return result.hashCode();
	}

	public boolean equals(Circle other) {
		if (this.radius != other.radius) return false;
		return true;
	}
}
