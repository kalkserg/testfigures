package figures;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CircleTest {
    Circle circle;

    @Before
    public void setUp(){
        circle = new Circle(12);
        System.out.println("Start test");
    }

    @Test
    public void souldPerimeterEquals() {
        final Double expected = 452.389342;
        final Double actual = circle.perimeter();
        assertEquals(expected, actual, 0.0001);
    }

    @Test
    public void souldAreaEquals() {
        final Double expected = 75.398223;
        final Double actual = circle.area();
        assertEquals(expected, actual, 0.0001);
    }

    @After
    public void tearDown() throws Exception{
        System.out.println("Test finished");
    }
    @AfterClass
    public static void afterClass() throws Exception{
        System.out.println("All tests finished");
    }
}