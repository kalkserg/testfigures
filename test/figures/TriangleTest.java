package figures;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {
    Triangle triangle;

    @Before
    public void setUp(){
        triangle = new Triangle(12,12,12);
        System.out.println("Start test");
    }

    @Test
    public void souldPerimeterEquals() {
        final Double expected = 36.;
        final Double actual = triangle.perimeter();
        assertEquals(expected, actual, 0.0001);
    }

    @Test
    public void souldAreaEquals() {
        final Double expected = 1.;
        final Double actual = triangle.area();
        assertEquals(expected, actual, 0.0001);
    }

    @After
    public void tearDown() throws Exception{
        System.out.println("Test finished");
    }
    @AfterClass
    public static void afterClass() throws Exception{
        System.out.println("All tests finished");
    }
}